#include<stdio.h>
#include<stdlib.h>
void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}void bubbleSort(int arr[], int n)
{
   int i, j;
   for (i = 0; i < n-1; i++)

       // Last i elements are already in place
       for (j = 0; j < n-i-1; j++)
           if (arr[j] > arr[j+1])
              swap(&arr[j], &arr[j+1]);
}
main()
{
    int i,a[100],n;
    printf("Enter the size of the array\n");
    scanf("%d",&n);
    printf("Enter the elements in the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("\nThe array is\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    bubbleSort(a,n);
    printf("\nThe array after sorting is\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    int count=1;
    int b[100];
    int k;
    i=0;
    int j=n-1;
    printf("\nj=%d",j);
    printf("\na[j]=%d",a[j]);
    for(k=0;k<n;k++)
    {
        if(count==1)
        {
            b[k]=a[j--];
        }
        else if(count%2==0)
        {
            b[k]=a[i++];
        }
        else
        {
            b[k]=a[j--];
        }
        count++;
    }
    printf("\nThe new array is\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",b[i]);
    }
}
