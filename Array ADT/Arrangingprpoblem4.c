#include<stdio.h>
void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}void bubbleSort(int arr[], int n)
{
   int i, j;
   for (i = 0; i < n-1; i++)

       // Last i elements are already in place
       for (j = 0; j < n-i-1; j++)
           if (arr[j] > arr[j+1])
              swap(&arr[j], &arr[j+1]);
}
int CountZeros(int a[],int n)
{
    int i;
    int count=0;
    for(i=0;i<n;i++)
    {
        if(a[i]==0)
        {
            count++;
        }
    }
    return count;
}
void rightshift(int a[],int n)
{
    int i;
    int temp = a[0];
    for(i=0;i<n;i++)
    {
        a[i]=a[i+1];
    }
    a[i]=temp;

}

main()
{
    int i,n,a[100];
    printf("Enter the size of the array\n");
    scanf("%d",&n);
    printf("Enter the elements in the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("\nThe array is \n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    for(i=0;i<n;i++)
    {
        if(a[i]!=0 && a[i+1]!=0)
        {
            a[i]=2*a[i];
            a[i+1]=0;
        }
    }
    printf("\nThe new array is\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    bubbleSort(a,n);
    printf("\nThe array after sorting becomes\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    int count = CountZeros(a,n);
    for(int i=0;i<count;i++)
    {
        rightshift(a,n);
    }
    printf("\nThe new array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
}
