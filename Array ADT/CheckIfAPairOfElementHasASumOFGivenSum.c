#include<stdio.h>
#include<stdlib.h>
int CheckBruteForce(int a[],int n,int s)
{
    int i,j;
    for(i=0;i<n-1;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if(a[i]+a[j]==s)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
void Bubble(int A[],int n)
{
    int i,j;
    int temp;
    for(i=0;i<n-1;i++)
    {
        for(j=0;j<n-1-i;j++)
        {
            if(A[j]>A[j+1])
            {
                temp=A[j];
                A[j]=A[j+1];
                A[j+1]=temp;
            }
        }
    }

}
int CheckSorting(int a[],int n,int s)
{
    int i,j;
    i=0;j=n-1;
    while(i<j)
    {
        if(a[i]+a[j]<s)
        {
            j--;
        }
        else if(a[i]+a[j]==s)
        {
            return 1;
            break;
        }
        else
        {
            i++;
        }
    }
}
main()
{
    int n;
    int a[100];
    int s;
    printf("Enter the number of elements you want to enter in the array\n");
    scanf("%d",&n);
    printf("Enter the elements in the array\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("\nEnter the sum you want to get\n");
    scanf("%d",&s);
    printf("\nChecking by Brute force\n");
    int r1=CheckBruteForce(a,n,s);
    if(r1==1)
    {
        printf("\nPair found\n");
    }
    else
    {
        printf("\nPair not found\n");
    }

    printf("\nChecking by sorting\n");
    Bubble(a,n);
    int r2=CheckSorting(a,n,s);
    if(r2==1)
    {
        printf("\nPair found\n");
    }
    else
    {
        printf("\nPair not found\n");
    }

}
