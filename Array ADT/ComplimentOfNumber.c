#include<stdio.h>
#include<stdlib.h>
#include<math.h>
main()
{
    int num;
    int num_bits;
    int mask;
    int result;
    printf("Enter the number whose compliment you want to calculate\n");
    scanf("%d",&num);
    num_bits = log2(num)+1;
    mask = (1<<num_bits)-1;
    result = num^mask;
    printf("\nThe compliment of the number is %d ",result);
}
