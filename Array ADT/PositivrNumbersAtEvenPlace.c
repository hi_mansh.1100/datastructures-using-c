#include<stdio.h>
#include<stdlib.h>
#include <errno.h>
void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}void bubbleSort(int arr[], int n)
{
   int i, j;
   for (i = 0; i < n-1; i++)

       // Last i elements are already in place
       for (j = 0; j < n-i-1; j++)
           if (arr[j] > arr[j+1])
              swap(&arr[j], &arr[j+1]);
}
main()
{
    int i,n,a[100];
    printf("Enter the size of the array\n");
    scanf("%d",&n);
    printf("\nEnter the array elements\n");
    int temp;
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("\nThe array is\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    bubbleSort(a,n);
    for(i=0;i<n;i++)
    {
        if(a[i+1]>0)
        {
            break;
        }
    }
    printf("\ni=%d\n",i);
    int j=0;
    int m=i;
    for(j=0;j<=m;j+2)
    {
        temp=a[i];
        a[i]=a[j];
        a[j]=temp;
    }
    printf("\nThe new array is\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
}
