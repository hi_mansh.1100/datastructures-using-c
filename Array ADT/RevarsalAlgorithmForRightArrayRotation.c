#include<stdio.h>
#include<stdlib.h>
void ReverseArray(int a[],int i,int j)
{
    int temp;
    while(i<j)
    {
        temp=a[i];
        a[i]=a[j];
        a[j]=temp;
        i++;
        j--;
    }
}
main()
{
    int n;
    int a[100];
    printf("\nEnter the number of elements in the array\n");
    scanf("%d",&n);
    printf("\nEnter the elements in the array\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("\nThe array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    printf("Enter the index at which you want the reversal to happen\n");
    int d;
    scanf("%d",&d);
    int i,j;
    ReverseArray(a,0,n-d-1);
    ReverseArray(a,d,n-1);
    ReverseArray(a,0,n-1);
    printf("\nArray after rotation\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }

}
