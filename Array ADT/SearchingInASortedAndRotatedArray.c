#include<stdio.h>
#include<stdlib.h>
int BinarySearch(int a[],int key,int l,int h)
{
    int mid;
    while(l<h)
    {
        mid=l+h/2;
        if(key==a[mid])
        {
            printf("Key found");
            return mid;
        }
        else if(key>a[mid])
        {
            l=mid+1;
        }
        else if(key<a[mid])
        {
            h=mid-1;
        }
    }
    return 0;
}
main()
{
    int n;
    int a[100];
    int key;
    printf("Enter the number of elements you want to add in the array\n");
    scanf("%d",&n);
    printf("Enter the elements into the array\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("\nThe given array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    int p;
    for(int i=0;i<n;i++)
    {
        if(a[i+1]<a[i])
        {
            p=i;
            break;
        }
    }
    printf("p=%d ",p);
    printf("\nEnter the number you want to search in the array\n");
    scanf("%d",&key);
    if(key<a[0])
    {
        int res=BinarySearch(a,key,0,p-1);
        if(res!=0)
        {
            printf("Key found at %d index",res);
        }
        else
        {
            printf("Key not found");
        }

    }
    else
    {
        int res=BinarySearch(a,key,p+1,n-1);
        if(res!=0)
        {
            printf("Key found at %d index",res);
        }
        else
        {
            printf("Key not found");
        }
    }

}
