#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *lchild;
    struct Node *rchild;
}*root;

struct queue
{
    int front;
    int rear;
    int size;
    struct Node **a;
};
void createq(struct queue *q,int size)
{
    q->front=q->rear=0;
    q->size=size;
    q->a=(struct Node **)malloc(q->size*sizeof(struct Node));
}
void enqueue(struct queue *q,struct Node *p)
{
    if((q->rear-1)%q->size==q->front)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->rear=(q->rear-1)%q->size;
        q->a[q->rear]=p;
    }
}
struct Node * dequeue(struct queue *q)
{
    struct Node *p;
    if(q->front==q->rear)
    {
        printf("Queue is empty\n");
    }
    else
    {
        q->front=(q->front-1)%q->size;
        p=q->a[q->front];
    }
    return p;
}
int IsEmpty(struct queue *q)
{
    return q->front==q->rear;
}
void CreateTree()
{
    struct queue q;
    createq(&q,100);
        int x;
    printf("Enter the data into the root node\n");
    scanf("%d",&x);
    root=(struct Node *)malloc(sizeof(struct Node));
    root->data=x;
    root->lchild=root->rchild=NULL;
    enqueue(&q,root);
    struct Node *p,*t;
    while(!IsEmpty(&q))
    {
        p=dequeue(&q);
        printf("Enter the left child of %d ",p->data);
        scanf("%d",&x);
        if(x!=-1 && x<p->data)
        {
            t=(struct Node *)malloc(sizeof(struct Node));
            t->data=x;
            t->lchild=t->rchild=NULL;
            p->lchild=t;
            enqueue(&q,t);
        }
        printf("Enter the right child of %d ",p->data);
        scanf("%d",&x);
        if(x!=-1 && x>p->data)
        {
            t=(struct Node *)malloc(sizeof(struct Node));
            t->data=x;
            t->lchild=t->rchild=NULL;
            p->rchild=t;
            enqueue(&q,t);
        }
    }
}
void Preorder(struct Node *p)
{
    if(p)
    {
        printf("%d ",p->data);
        Preorder(p->lchild);
        Preorder(p->rchild);
    }
}
void Inorder(struct Node *p)
{
    if(p)
    {
        Inorder(p->lchild);
        printf("%d ",p->data);
        Inorder(p->rchild);
    }
}
void Postorder(struct Node *p)
{
    if(p)
    {
        Postorder(p->lchild);
        Postorder(p->rchild);
        printf("%d ",p->data);
    }
}
void IInsert(struct Node *t,int key)
{
    struct Node *r=NULL,*p;
    while(t!=NULL)
    {
        r=t;
        if(t->data==key)
        {
            return ;
        }
        else if(t->data<key)
        {
            t=t->rchild;
        }
        else if(t->data>key)
        {
            t=t->lchild;
        }
        else
        {
            printf("\nKey not found\n");
        }
    }
    p=(struct Node *)malloc(sizeof(struct Node));
    p->data=key;
    p->lchild=p->rchild=NULL;
    if(r->data>key)
    {
        r->lchild=p;
    }
    else
    {
        r->rchild=p;
    }
}
main()
{
    CreateTree();
    printf("\nThe preorder traversal of the binary search tree is\n");
    Preorder(root);
    printf("\nThe inorder traversal of the binary search tree is\n");
    Inorder(root);
    printf("\nThe postorder traversal of the binary search tree is\n");
    Postorder(root);
    int key;
    printf("\nEnter the number you want to insert in the binary search tree\n");
    scanf("%d",&key);
    IInsert(root,key);
    Preorder(root);

}

