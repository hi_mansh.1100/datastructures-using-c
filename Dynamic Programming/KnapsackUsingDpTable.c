#include<stdio.h>
#include<stdlib.h>
int DP[3][4];
int max(int a,int b)
{
    if(a>b)
    {
        return a;
    }
    else
    {
        return b;
    }
}
void Knapsack(int wt[],int profit[],int w,int n)
{
    int i,j;
    for(i=0;i<n;i++)
    {
        for(j=0;j<w;j++)
        {
            if(i==0 || j==0)
            {
                DP[i][j]=0;
            }
            else if(wt[i-1]>j)
            {
                DP[i][j]=DP[i-1][j];
            }
            else
            {
                DP[i][j]=max(DP[i][j]=DP[i-1][j],(profit[i-1]+DP[i-1][w-wt[i-1]]));
            }
        }
    }

    printf("\nThe maximum profit is %d",DP[3][4]);
}
main()
{
    int w,n,i,j;
    int wt[100];
    int profit[100];
    printf("Enter the number of items you want\n");
    scanf("%d",&n);
    printf("\nEnter the weights of each item\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&wt[i]);
    }
    printf("\nEnter the profit of each element\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&profit[i]);
    }
    for(i=0;i<n;i++)
    {
        for(j=0;j<w;j++)
        {
            DP[i][j]=-1;
        }
    }
    Knapsack(wt,profit,w,n);
}
