#include<stdio.h>
#include<stdlib.h>
int DP[3][4];
int max(int a,int b)
{
    if(a>b)
    {
        return a;
    }
    else
    {
        return b;
    }
}
int Knapsack(int wt[],int profit[],int w,int n)
{
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<w;j++)
        {
            printf("%d\t",DP[i][j]);
        }
        printf("\n");
    }
    int result;
    printf("%d",w);
    printf("\n%d",n);
    if(w==0 || n==0)
    {
        return 0;
    }
    if(DP[w][n]!=-1)
    {
        return DP[w][n];
    }
    else if(wt[n]>w)
    {
        result = Knapsack(wt,profit,w,n-1);
    }
    else
    {
        DP[w][n]=max(Knapsack(wt,profit,w,n-1),(profit[n]+Knapsack(wt,profit,w-wt[n],n-1)));
    }



}

main()
{
    int w,n;
    printf("Enter the number of items you want to add\n");
    scanf("%d",&n);
    printf("\nEnter the maximum weight of the knapsack\n");
    scanf("%d",&w);
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<w;j++)
        {
            DP[i][j]=-1;
        }
    }
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<w;j++)
        {
            printf("%d\t",DP[i][j]);
        }
        printf("\n");
    }
    int wt[n];
    int profit[n];
    printf("\nEnter the weights of each item\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&wt[i]);
    }
    printf("\nEnter the profit of each item\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&profit[i]);
    }
    printf("The maximum profit is %d ",Knapsack(wt,profit,w,n));
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<w;j++)
        {
            printf("%d\t",DP[i][j]);
        }
        printf("\n");
    }

}
