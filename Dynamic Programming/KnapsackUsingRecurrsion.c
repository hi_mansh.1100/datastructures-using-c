#include<stdio.h>
#include<stdlib.h>
int max(int a,int b)
{
    if(a>b)
    {
        return a;
    }
    else
    {
        return b;
    }
}
int Knapsack(int wt[],int profit[],int w,int n)
{
    if(w==0 || n==0)
    {
        return 0;
    }
    else if(wt[n]>w)
    {
        return Knapsack(wt,profit,w,n-1);
    }
    else if(wt[n]<w)
    {
        return max(Knapsack(wt,profit,w,n-1),(profit[n]+Knapsack(wt,profit,w-wt[n],n-1)));
    }
}
main()
{
    int wt[10];
    int profit[10];
    int w;
    int n;
    printf("Enter the number of items\n");
    scanf("%d",&n);
    printf("\nEnter the total weight of the bag\n");
    scanf("%d",&w);
    printf("Enter the weights of every item\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&wt[i]);
    }
    printf("\nEnter the profit of every item\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&profit[i]);
    }
    printf("\nThe maximum profit is %d ",Knapsack(wt,profit,w,n));
}
