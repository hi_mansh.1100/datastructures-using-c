#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*head2=NULL,*last=NULL,*last2=NULL;
void create(int key)
{
    struct Node *p,*q;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=key;
        last=head;
        head->next=NULL;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=key;
        p->next=NULL;
        last->next=p;
        last=p;
    }
}
void create2(int key)
{
    struct Node *p,*q;
    if(head2==NULL)
    {
        head2=(struct Node*)malloc(sizeof(struct Node));
        head2->data=key;
        last2=head2;
        head2->next=NULL;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=key;
        p->next=NULL;
        last2->next=p;
        last2=p;
    }
}
void Display()
{
    struct Node*p;
    if(head!=NULL)
    {
        p=head;
        while(p!=NULL)
        {
            printf("%d->",p->data);
            p=p->next;
        }
    }
}
void Display2()
{
    struct Node*p;
    if(head2!=NULL)
    {
        p=head2;
        while(p!=NULL)
        {
            printf("%d->",p->data);
            p=p->next;
        }
    }
}
void Concat()
{
    struct Node *p;
    p=head;
    while(p->next!=NULL)
    {
        p=p->next;
    }
    p->next=head2;
}
main()
{
    printf("\nEnter the number of element you want to add to the first linked list\n");
    int n;
    int key;
    scanf("%d",&n);
    printf("Enter the elements into the linked list\n");
    for(int i=0;i<n;i++)
    {
        key=0;
        scanf("%d",&key);
        create(key);

    }
    Display();
    printf("\nEnter the number of element you want to add to the second linked list\n");
    int n2;
    int keyy;
    scanf("%d",&n2);
    printf("\nEnter the elements into the second linked list\n");
    for(int i=0;i<n2;i++)
    {
        keyy=0;
        scanf("%d",&keyy);
        create2(keyy);

    }
    Display2();
    printf("\nAfter concatination the linked list looks like\n");
    Concat();
    Display();
}

