#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;
void create(int key)
{
    struct Node *p,*q;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=key;
        last=head;
        head->next=NULL;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=key;
        p->next=NULL;
        last->next=p;
        last=p;
    }
}
void Display()
{
    struct Node*p;
    if(head!=NULL)
    {
        p=head;
        while(p!=NULL)
        {
            printf("%d->",p->data);
            p=p->next;
        }
    }
}
void InsertAtB(int num)
{
    struct Node *p;
    p=(struct Node *)malloc(sizeof(struct Node));
    p->data=num;
    p->next=head;
    head=p;
}
void InsertAtE(int num)
{
    struct Node *p;
    p=(struct Node *)malloc(sizeof(struct Node));
    p->data=num;
    p->next=NULL;
    last->next=p;
    last=p;
}
void InsertAtP(int num,int pos)
{
    struct Node *p,*q;
    q=-1;
    p=head;
    for(int i=0;i<pos-1;i++)
    {
        q=p;
        p=p->next;
    }
    p=(struct Node *)malloc(sizeof(struct Node));
    p->data=num;
    p->next=q->next;
    q->next=p;
}
main()
{
    printf("Enter the number of element you want to add to the linked list\n");
    int n;
    int key;
    scanf("%d",&n);
    printf("Enter the elements into the linked list\n");
    for(int i=0;i<n;i++)
    {
        key=0;
        scanf("%d",&key);
        create(key);

    }
    Display();
    int num;
    printf("\nEnter the number you want to insert in the linkedlist\n");
    scanf("%d",&num);
    InsertAtB(num);
    printf("\nAfter inserting %d in the beginning the linked list looks like\n",num);
    Display();
    InsertAtE(num);
    printf("\nAfter inserting %d in the end the linked list looks like\n",num);
    Display();
    printf("\nEnter the position at which you want to insert\n");
    int pos;
    scanf("%d",&pos);
    InsertAtP(num,pos);
    printf("\nAfter inserting %d at position %d the linked list looks like\n",num,pos);
    Display();


}

