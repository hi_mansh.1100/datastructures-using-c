#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;
void insert(int num)
{
    struct Node *p;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=num;
        head->next=NULL;
        last=head;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=num;
        p->next=NULL;
        last->next=p;
        last=p;

    }
}
void Display()
{
    struct Node *p;

        p=head;
        while(p)
        {
            printf("%d ",p->data);
            p=p->next;
        }
}
void GetNthNodeFromBeginning(int n)
{
    struct Node *p;
    p=head;
    int count=0;
    while(p!=NULL)
    {
        if(count==n)
        {
            printf("The Nth node from the beginning is %d ",p->data);
        }
        count++;
        p=p->next;
    }
}

main()
{
    int size;
    printf("Enter the number of element you want to add in the linked list\n");
    scanf("%d",&size);
    printf("Enter the element into the linked list\n");
    int num;
    for(int i=0;i<size;i++)
    {
        num=0;
        scanf("%d",&num);
        insert(num);
    }
    Display();
    int n;
    printf("\nEnter the index you want to get\n");
    scanf("%d",&n);
    GetNthNodeFromBeginning(n);
    fflush(stdin);
}
