#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;
void insert(int num)
{
    struct Node *p;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=num;
        head->next=NULL;
        last=head;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=num;
        p->next=NULL;
        last->next=p;
        last=p;

    }
}
void Display()
{
    struct Node *p;

        p=head;
        while(p)
        {
            printf("%d ",p->data);
            p=p->next;
        }
}
void RemoveDuplicate()
{
    struct Node *p,*q;
    p=head;
    while(p->next!=NULL)
    {

        if(p->data==p->next->data)
        {
            q=p->next->next;
            free(p);
            p->next=q;
        }
        else
        {
            p=p->next;

        }
    }

}
main()
{
    int size;
    printf("Enter the number of element you want to add in the linked list\n");
    scanf("%d",&size);
    printf("Enter the element into the linked list\n");
    int num;
    for(int i=0;i<size;i++)
    {
        num=0;
        scanf("%d",&num);
        insert(num);
    }
    Display();
    RemoveDuplicate();
    printf("\n");
    Display();


}
