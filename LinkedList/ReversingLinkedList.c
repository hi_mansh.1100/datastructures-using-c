#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;
void create(int key)
{
    struct Node *p,*q;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=key;
        last=head;
        head->next=NULL;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=key;
        p->next=NULL;
        last->next=p;
        last=p;
    }
}
void Display()
{
    struct Node*p;
    if(head!=NULL)
    {
        p=head;
        while(p!=NULL)
        {
            printf("%d->",p->data);
            p=p->next;
        }
    }
}
void Reverse()
{
    struct Node *p,*q,*r;
    p=head;
    q=r=NULL;
    while(p!=NULL)
    {
        r=q;
        q=p;
        p=p->next;
        q->next=r;
    }
    head=q;
}
main()
{
    printf("Enter the number of element you want to add to the linked list\n");
    int n;
    int key;
    scanf("%d",&n);
    printf("Enter the elements into the linked list\n");
    for(int i=0;i<n;i++)
    {
        key=0;
        scanf("%d",&key);
        create(key);

    }
    Display();
    printf("\nAfter reversing the linked list we get\n");
    Reverse();
    Display();
}

