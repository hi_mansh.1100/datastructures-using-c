#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;
void create(int key)
{
    struct Node *p,*q;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=key;
        last=head;
        head->next=NULL;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=key;
        p->next=NULL;
        last->next=p;
        last=p;
    }
}
void Display()
{
    struct Node*p;
    if(head!=NULL)
    {
        p=head;
        while(p!=NULL)
        {
            printf("%d->",p->data);
            p=p->next;
        }
    }
}
void search(int num)
{
    struct Node *p;
    p=head;
    int i=0;
    int flag=0;
    while(p!=NULL)
    {
        i++;
        if(p->data==num)
        {
            flag=1;
            break;
        }
        else
        {
            flag =0;
            p=p->next;
        }
    }
    if(flag ==0)
    {
        printf("Element you entered is not found in the linked list\n");
    }
    else
    {
        printf("The element is found at %d position",i);
    }
}
main()
{
    printf("Enter the number of element you want to add to the linked list\n");
    int n;
    int key;
    scanf("%d",&n);
    printf("Enter the elements into the linked list\n");
    for(int i=0;i<n;i++)
    {
        key=0;
        scanf("%d",&key);
        create(key);

    }
    Display();
    int num;
    printf("\nEnter the number you want to search in the linked list\n");
    scanf("%d",&num);
    search(num);
}

