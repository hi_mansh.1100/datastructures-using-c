#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;
void create(int key)
{
    struct Node *p,*q;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=key;
        last=head;
        head->next=NULL;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=key;
        p->next=NULL;
        last->next=p;
        last=p;
    }
}
int Sum()
{
    struct Node*p;
    int sum=0;
    if(head!=NULL)
    {
        p=head;
        while(p!=NULL)
        {
            sum=sum+p->data;
            p=p->next;
        }
    }
    return sum;
}
main()
{
    printf("Enter the number of element you want to add to the linked list\n");
    int n;
    int key;
    scanf("%d",&n);
    printf("Enter the elements into the linked list\n");
    for(int i=0;i<n;i++)
    {
        key=0;
        scanf("%d",&key);
        create(key);

    }
    printf("The sum of all the nodes is %d ",Sum());
}

