#include<stdio.h>
#include<stdlib.h>
struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;
void insert(int num)
{
    struct Node *p;
    if(head==NULL)
    {
        head=(struct Node*)malloc(sizeof(struct Node));
        head->data=num;
        head->next=NULL;
        last=head;
    }
    else
    {
        p=(struct Node*)malloc(sizeof(struct Node));
        p->data=num;
        p->next=NULL;
        last->next=p;
        last=p;

    }
}
void Display()
{
    struct Node *p;

        p=head;
        while(p)
        {
            printf("%d ",p->data);
            p=p->next;
        }
}
void Swap(int k1,int k2)
{
    struct Node *p,*q;
    p=head;
    q=head;
    struct Node *a,*b;
    a=NULL;
    b=NULL;
    while(p->data!=k1)
    {
        a=p;
        p=p->next;
    }
    while(q->data!=k2)
    {
        b=q;
        q=q->next;
    }
    a->next=q;
    q->next=a->next->next;
    b->next=p;
    p->next=b->next->next;
}
main()
{
    int size;
    printf("Enter the number of element you want to add in the linked list\n");
    scanf("%d",&size);
    printf("Enter the element into the linked list\n");
    int num;
    for(int i=0;i<size;i++)
    {
        num=0;
        scanf("%d",&num);
        insert(num);
    }
    Display();
    printf("\nEnter two number you want to swap from the list");
    int n1,n2;
    scanf("%d%d",&n1,&n2);
    Swap(n1,n2);
    Display();
}
