#include<stdio.h>
#include<stdlib.h>

struct queue
{
    int front;
    int rear;
    int size;
    int *a;
};
void create(struct queue *q,int size)
{
    q->size=size;
    q->front=q->rear=0;
    q->a=(int *)malloc(q->size*sizeof(int));
}
void enqueue(struct queue *q,int key)
{
    if((q->rear+1)%q->size==q->front)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->rear=(q->rear+1)%q->size;
        q->a[q->rear]=key;
    }
}
int dequeue(struct queue *q)
{
    int x=-1;
    if(q->front==q->rear)
    {
        printf("Queue is empty\n");
    }
    else
    {
        q->front=(q->front+1)%q->size;
        x=q->a[q->front];
    }
    return x;
}
void Display(struct queue *q)
{
    int i=q->front+1;
    do
    {
        printf("%d\n",q->a[i]);
        i=(i+1)%q->size;
    }while(i!=(q->rear+1)%q->size);
}
main()
{
    struct queue *q;
    create(&q,100);
    int choice;
    int quit=0;
    int num=0;
    while(1)
    {
        choice=0;
        printf("Enter the choice you want to perform in the queue\n1.Enqueue\n2.Dequeue\n3.Display\n4.Quit\n");
        scanf("%d",&choice);
        switch(choice)
        {
        case 1:
            printf("\nEnter the number you want to enqueue\n");
            scanf("%d",&num);
            enqueue(&q,num);
            break;
        case 2:
            printf("The number dequeued is %d \n",dequeue(&q));
            break;
        case 3:
            Display(&q);
            break;
        case 4:
            exit(1);
            break;
        }
    }

}
