#include<stdio.h>
#include<stdlib.h>

struct queue
{
    int front;
    int rear;
    int size;
    int *a;
};
void create(struct queue *q)
{

    printf("Enter the size of the queue you want to create\n");
    scanf("%d",&q->size);
    q->front=q->rear=-1;
    q->a=(int *)malloc(q->size*sizeof(int));

}
void enqueue(struct queue *q,int num)
{
    if(q->rear==q->size-1)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->a[q->rear++]=num;
    }
}
int dequeue(struct queue *q)
{
    int x=-1;
    if(q->front==q->rear)
    {
        printf("Queue is empty\n");
    }
    else
    {
        x=q->a[q->front++];
    }
    return x;
}
void Display(struct queue *q)
{
    for(int i=q->front;i<=q->rear;i++)
    {
        printf("%d\t",q->a[i]);
    }
}
main()
{
    struct queue q;
    create(&q);
    printf("Enter the element you want to add in the queue\n");
    int num;
    for(int i=0;i<q.size;i++)
    {
        num=0;
        scanf("%d",&num);
        enqueue(&q,num);
    }
    Display(&q);
    printf("\nElement popped is %d",dequeue(&q));
    printf("\nElement popped is %d",dequeue(&q));
     printf("\nElement popped is %d",dequeue(&q));
      printf("\nElement popped is %d",dequeue(&q));
       printf("\nElement popped is %d",dequeue(&q));
}
