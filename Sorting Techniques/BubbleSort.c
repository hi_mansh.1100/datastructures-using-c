#include<stdio.h>
#include<stdlib.h>

void Bubble(int A[],int n)
{
    int i,j;
    int temp;
    for(i=0;i<n-1;i++)
    {
        for(j=0;j<n-1-i;j++)
        {
            if(A[j]>A[j+1])
            {
                temp=A[j];
                A[j]=A[j+1];
                A[j+1]=temp;
            }
        }
    }

}
main()
{
    int n;
    printf("Enter the size of the array\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the elements in the array\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("The original array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    Bubble(a,n);
    printf("\nThe Sorted array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }

}
