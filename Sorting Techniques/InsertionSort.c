#include<stdio.h>
#include<stdlib.h>
void InsertionSort(int a[],int n)
{
    int i,j,x=0;
    for(i=1;i<n-1;i++)
    {
        j=i-1;
        x=a[i];
        while(j>=0&&a[j]>x)
        {
            a[j+1]=a[j];
            j--;
        }
        a[j+1]=x;
    }
}
main()
{

    int n;
    printf("Enter the size of the array\n");
    scanf("%d",&n);
    int a[n];
    printf("\nEnter the elements in the array\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("The original array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    InsertionSort(a,n);
    printf("\nThe sorted array is\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
}
