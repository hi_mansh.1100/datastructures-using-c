#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct stack
{
    int top;
    int size;
    char *a;
};
void create(struct stack *s)
{
    s->top=-1;
    s->size=100;
    s->a=(char *)malloc(s->size*sizeof(char));

}
void Push(struct stack *s,char exp)
{
    if(s->top==s->size-1)
    {
        printf("Stack overflow\n");
    }
    else
    {
        s->a[s->top]=exp;
    }
}
char Pop(struct stack *s)
{
    char x;
    if(s->top==-1)
    {
        printf("Stack Underflow\n");
    }
    else
    {
        x=s->a[s->top++];
    }
    return x;

}
int IsBalanced(struct stack *s,char *exp)
{
    int i;
    for(i=0;exp[i]!='\0';i++)
    {
        if(exp[i]=="(")
        {
            Push(&s,exp[i]);
        }
        else if(exp[i]==")")
        {
            if(s->top==-1)
            {
                return 0;
            }
            else
            {
                Pop(&s);
                s->top--;
            }
        }
    }
    if(s->top==-1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
main()
{
    struct stack s;
    create(&s);
    char exp[100];
    printf("Enter the expression\n");
    gets(exp);
    int result = IsBalanced(&s,exp);
    if(result==1)
    {
        printf("The expression is balanced\n");
    }
    else if(result==0)
    {
        printf("The expression is not balanced\n");
    }

}
