#include<stdio.h>
#include<stdlib.h>

struct stack
{
    int top;
    int size;
    int *a;
};
void create(struct stack *s)
{
    printf("Enter the size of the stack\n");
    int size;
    scanf("%d",&size);
    s->size=size;
    s->top=-1;
    s->a=(int *)malloc(size*sizeof(int));
}
void Push(struct stack *s,int num)
{

    if(s->top>s->size-1)
    {
        printf("Stack overflow\n");
    }
    else
    {
        s->top++;
        s->a[s->top]=num;
    }
}
int Pop(struct stack *s)
{
    int x=-1;
    if(s->top==-1)
    {
        printf("Stack underflow\n");
    }
    else
    {
        x=s->a[s->top--]=x;
    }
    return x;
}
void Display(struct stack *s)
{
    int i;
    for(i=s->top;i>=0;i--)
    {
        printf("%d\n",s->a[i]);

    }
}
void Peek(struct stack *s,int index)
{
    printf("%d",s->a[index]);
}
int IsEmpty(struct stack *s)
{
    if(s->top==-1)
    {
        return 1
    }
    else
    {
        return 0;
    }
}
int IsFull(struct stack *s)
{
    if(s->top==s->size-1)
    {
        return 1
    }
    else
    {
        return 0;
    }
}
main()
{
    struct stack s;
    create(&s);
    int num;
    printf("Enter number in the stack\n");
    for(int i=0;i<s.size;i++)
    {
        num=0;
        scanf("%d",&num);
        Push(&s,num);
    }
    printf("\nThe stack is\n");
    Display(&s);
    int index;
    printf("Enter the index at which you want to peek\n");
    scanf("%d",&index);
    Peek(&s,index);

}
