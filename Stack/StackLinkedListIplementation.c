#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *next;
}*head=NULL,*last=NULL;

void Push(int key)
{
    struct Node *p;
    if(head==NULL)
    {
        head=(struct Node *)malloc(sizeof(struct Node));
        head->data=key;
        head->next=NULL;
        last=head;
        last->next=NULL;
    }
    else
    {
        p=(struct Node *)malloc((sizeof(struct Node)));
        p->data=key;
        p->next=NULL;
        last->next=p;
        last=p;
    }
}
void Display()
{
    struct Node *p;
    if(head!=NULL)
    {
        p=head;
        while(p)
        {
            printf("%d\n",p->data);
            p=p->next;
        }
    }
}
void Pop()
{
    struct Node *p;
    p=last;
    int x;
    x=p->data;
    free(p);
    printf("%d",x);
}
main()
{
    printf("Enter the number of element you want to add in the stack\n");
    int n;
    scanf("%d",&n);
    int num;
    printf("Enter the number you want to push in the stack\n");
    for(int i=0;i<n;i++)
    {
        num=0;
        scanf("%d",&num);
        Push(num);
    }
    Display();
}
