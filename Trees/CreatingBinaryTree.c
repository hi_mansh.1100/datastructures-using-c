#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *lchild;
    struct Node *rchild;
}*root=NULL;

struct queue
{
    int front;
    int rear;
    int size;
    struct Node **a;
};

void createq(struct queue *q,int size)
{
    q->front=q->rear=0;
    q->size=size;
    q->a=(struct Node **)malloc(q->size*sizeof(struct Node *));
}
void enqueue(struct queue *q,struct Node *p)
{
    if((q->rear+1)%q->size==q->front)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->rear=(q->rear+1)%q->size;
        q->a[q->rear]=p;
    }
}
struct Node * dequeue(struct queue *q)
{
    struct Node *p=NULL;
    if(q->front==q->rear)
    {
        printf("Queue is empty\n");
    }
    else
    {
        q->front=(q->front+1)%q->size;
        p=q->a[q->front];
    }
    return p;
}
int IsEmpty(struct queue *q)
{
    return q->front==q->rear;
}
void CreateTree()
{
    struct queue q;
    createq(&q,100);
    struct Node *p,*t;
    int x;
    printf("\nEnter the data in to the root node\n");
    scanf("%d",&x);
    root=(struct Node *)malloc(sizeof(struct Node));
    root->data=x;
    root->lchild=root->rchild=NULL;
    enqueue(&q,root);
    int num;
    while(!IsEmpty(&q))
    {
        num=0;
        p=dequeue(&q);
        printf("\nEnter the left child of %d \n",p->data);
        scanf("%d",&num);
        if(num!=-1)
        {
            t=(struct Node *)malloc(sizeof(struct Node));
            t->data=num;
            t->lchild=t->rchild=NULL;
            p->lchild=t;
            enqueue(&q,t);
        }
        printf("\nEnter the Right child of %d \n",p->data);
        scanf("%d",&num);
        if(num!=-1)
        {
            t=(struct Node *)malloc(sizeof(struct Node));
            t->data=num;
            t->lchild=t->rchild=NULL;
            p->rchild=t;
            enqueue(&q,t);
        }

    }
}
void Preorder(struct Node *p)
{
    if(p)
    {
        printf("%d\t",p->data);
        Preorder(p->lchild);
        Preorder(p->rchild);
    }
}
void Inorder(struct Node *p)
{
    if(p)
    {
        Inorder(p->lchild);
        printf("%d\t",p->data);
        Inorder(p->rchild);
    }
}
void PostOrder(struct Node *p)
{
    if(p)
    {
        PostOrder(p->lchild);
        PostOrder(p->rchild);
        printf("%d\t",p->data);
    }
}
main()
{
    CreateTree();
    printf("\nThe preorder tree traversal is\n");
    Preorder(root);
    printf("\nInorder Traversal of the tree is\n");
    Inorder(root);
    printf("\nPostorder traversal of the tree is\n");
    PostOrder(root);

}
