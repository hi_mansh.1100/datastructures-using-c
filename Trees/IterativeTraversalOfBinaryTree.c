#include<stdio.h>
#include<stdlib.h>

struct Node
{
    int data;
    struct Node *lchild;
    struct Node *rchild;
}*root;

struct queue
{
    int front;
    int rear;
    int size;
    struct Node **a;
};
struct stack
{
    int top;
    int size;
    struct Node **s;
};
void CreateStack(struct stack *s,int size)
{
    s->size=size;
    s->top=-1;
    s->s=(struct Node *)malloc(sizeof(struct Node *));
}
void Push(struct stack *s,struct Node *p)
{
    if(s->top==s->size-1)
    {
        printf("Stack Overflow\n");
    }
    else
    {
        s->s[s->top++]=p;
    }
}
struct Node * Pop(struct stack *s)
{
    struct Node *p;
    if(s->top==-1)
    {
        printf("stack Underflow\n");
    }
    else
    {
        p=s->s[s->top--];
    }
}
int IsEmptyStack(struct stack s)
{
    return s.top==-1;
}
void createq(struct queue *q,int size)
{
    q->a=(struct Node **)malloc(q->size*sizeof(struct Node *));
    q->size=size;
    q->front=q->rear=0;
}
void enqueue(struct queue *q,struct Node *p)
{
    if((q->rear+1)%q->size==q->front)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->rear=(q->rear+1)%q->size;
        q->a[q->rear]=p;
    }
}
struct Node * dequeue(struct queue *q)
{
    struct Node *p;
    if(q->front==q->rear)
    {
        printf("Queue is full\n");
    }
    else
    {
        q->front=(q->front+1)&q->size;
        p=q->a[q->front];
    }
    return p;
}
int IsEmpty(struct queue *q)
{
    return q->front==q->rear;
}
void CreateTree()
{
    struct queue q;
    createq(&q,100);
    struct Node *p,*t;
    int x;
    printf("\nEnter the data in to the root node\n");
    scanf("%d",&x);
    root=(struct Node *)malloc(sizeof(struct Node));
    root->data=x;
    root->lchild=root->rchild=NULL;
    enqueue(&q,root);
    int num;
    while(!IsEmpty(&q))
    {
        num=0;
        p=dequeue(&q);
        printf("\nEnter the left child of %d \n",p->data);
        scanf("%d",&num);
        if(num!=-1)
        {
            t=(struct Node *)malloc(sizeof(struct Node));
            t->data=num;
            t->lchild=t->rchild=NULL;
            p->lchild=t;
            enqueue(&q,t);
        }
        printf("\nEnter the Right child of %d \n",p->data);
        scanf("%d",&num);
        if(num!=-1)
        {
            t=(struct Node *)malloc(sizeof(struct Node));
            t->data=num;
            t->lchild=t->rchild=NULL;
            p->rchild=t;
            enqueue(&q,t);
        }

    }
}
void IterativePreorder(struct Node *p)
{
    struct stack s;
    CreateStack(&s,100);
    while(p!=NULL || IsEmptyStack(s))
    {
        if(p!=NULL)
        {
            printf("%d\t",p->data);
            Push(&s,p);
            p=p->lchild;
        }
        else
        {
            p=Pop(&s);
            p=p->rchild;
        }

    }
}
main()
{
    CreateTree();
    IterativePreorder(root);
}
